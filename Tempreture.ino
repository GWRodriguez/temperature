const int sensorPin = A0;
const float baseline = 20.0;


void setup() {
  // lets you monitor the serial input in the serial monitor
  Serial.begin(9600);

  for (int pin = 2; pin < 5; pin++) {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
  }
}


void loop() {
  int sensorVal = analogRead(sensorPin);

  Serial.print("Serial value: ");
  Serial.print(sensorVal);

  // converts readings to 5V
  float voltage = (sensorVal / 1024.0) * 5.0;

  Serial.print(", Volts: ");
  Serial.print(voltage);

  // convert voltage to celcius
  float temp = (voltage - 0.5) * 100;
  Serial.print(", Tempreture: ");
  Serial.println(temp);

  if (temp < baseline) {
    digitalWrite(2, LOW);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
  }
  else if (temp >= baseline && temp < baseline + 5) {
    digitalWrite(2, HIGH);
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
  }
  else if (temp >= baseline + 5 && temp < baseline + 10) {
    digitalWrite(2, HIGH);
    digitalWrite(3, HIGH);
    digitalWrite(4, LOW);
  }
  else {
    digitalWrite(2, HIGH);
    digitalWrite(3, HIGH);
    digitalWrite(4, HIGH);
  }

  delay(1000);
}









